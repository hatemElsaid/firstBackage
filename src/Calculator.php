<?php

namespace Calculator;


class Calculator
{


    /* add */
    public function plus($x, $y)
    {
        return $x + $y ;
    }


    /* subtract */
    public function subtract($x, $y)
    {
        return $x - $y ;
    }

    /* multi */
    public function mult($x, $y)
    {
        return $x * $y ;
    }

    /* div */
    public function div($x, $y)
    {
        return $x / $y ;
    }

    //
    public function mod($x, $y)
    {
        return $x % $y ;
    }

    //power
    public function power($x, $y)
    {
        return pow($x , $y) ;
    }

    //log

    public function log($x, $y)
    {
        return log($x , $y) ;
    }

}